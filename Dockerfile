FROM python:3.7-slim
RUN mkdir /code
WORKDIR /code/
ADD ./app/ /code/
RUN ls
RUN apt-get update && apt-get  --no-install-recommends install -y python3-opencv
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
EXPOSE 4000 



